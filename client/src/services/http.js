import axios from "axios";
const config = {
	apiUrl: "http://localhost:4000"
};

const agent = axios.create({
	baseURL: config.apiUrl,
	headers: {
		"Content-Type": "application/json"
	}
});

export default {
    getTasks: () => agent.get("task"),
    addTask: body => agent.post('task', body),
    updateTask: (id, data) => agent.put(`task/${id}`, data),
    deleteTask: id => agent.delete(`task/${id}`)
}