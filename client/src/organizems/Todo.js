import React from 'react';

import agent from '../services/http'
import '../index.css'
import Input from '../molecules/Input';
import Button from '../atoms/Button';
import { useForm } from 'react-hook-form';


export default function Todo() {
    
  const {register,handleSubmit,formState:{errors}}= useForm()

  const [todoList,setTodoList] = React.useState([])
  const [updateValue,setUpdateValue] = React.useState(null)


  const submitForm = async (data,e)=> {
   
    if(updateValue) {
        const response = await agent.updateTask(updateValue._id,data)
        const updatedTodos = todoList.filter(todo => todo._id !==updateValue._id)
        setTodoList([response.data.data,...updatedTodos])
        setUpdateValue(null)
        alert('Value Updated Succesfully')
        

    } else {
        try {
            const response = await agent.addTask(data)
            setTodoList([response.data.data,...todoList])
            alert('Value Added Succesfully')
        } catch (e) {
            alert(`Error Adding Record!`)
           
        }
    }
    e.target.reset()
    
  }
  React.useEffect(async() => {
    const response = await agent.getTasks()
    setTodoList(response.data.data)
  
  }, [])


  const deleteTodo = async (todo)=> {
   const deleted = await agent.deleteTask(todo._id)
   alert('Value Delted Succesfully')
  }
  
  const updateTodo = data => setUpdateValue(data)
   
 
  return (
    <div className="mt-10 md:w-3/12  bg-gray-300 container mx-auto">
      <form
       className="md:flex items-center"
     
      onSubmit={handleSubmit(submitForm)}
      >
       
      <Input
        
        defaultValue={updateValue?.todo}
        name="todo"
        type="text"
        placeholder="Enter Todo"
        id="todo"
        label="Add Todo"
        autoFocus={updateValue ? true:false}
        className="responsive md:grid-cols-6 md:m-4 rounded-full focus:ring-purple-600 focus:border-transparent"
        errmsg={errors?.todo?.message}
        {...register('todo',{required:'This Field is Required',id:'todo'})}
      />
   
      <Button
       type="submit"
       name={updateValue ? 'Update' : 'Add'}
       className="md:m-4 md:place-self-end btn btn-green add-btn"
      />
      </form>
     <div className="m-5">
       <h1 className="md:ml-60 md:mb-5 font-extrabold heading">TODOS LIST</h1>
       {todoList.map(data =>(
        <div 
        key={data._id}
        className="md:grid md:grid-cols-3 gap- mb-2"
        style={{borderBottom:"1px solid yellow"}}
        >
         <p  className="md:font-bold todo-content"> {data.todo} </p>
        <div className="btnContainer" style={{overflow:'hidden'}}>
        <Button
      
      name="Update"
      onClick={() =>updateTodo(data)}
      className="btn mob-btn md:mr-10 btn-blue felx w-12/12"
     />
     <Button
    
      name="Delete"
      onClick={() =>deleteTodo(data)}
      className="btn mob-btn2 btn-red felx"
     />
       </div>
        </div>
       ))}
     </div>
    </div>
  );
}
