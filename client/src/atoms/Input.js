import React from 'react'

export default function Input(props) {
    const {type,id,label,placehoder,...rest} = props
    
    return (
        <div className="flex md:inline-flex">
            <input 

               defaultValue={props.defaultValue}
                {...props}
            />
        </div>
    )
}
