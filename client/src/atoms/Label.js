import React from 'react'

export default ({id,label}) => <label className="md:m-4 text-size-700 md:font-bold" htmlFor={id}>{label}:</label>
        
    

