import React from 'react'

export default function Button(props) {
    const {name , onClick, className,...rest} = props
    return (
        
            <button
             name={name}
             onClick={onClick}
             className={className}
             onSubmit={props.onSubmit}
             {...rest}
            >
                {name}
            </button>
        
    )
}
