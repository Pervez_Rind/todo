
import React from 'react'

import Input from '../atoms/Input'
import ErrorMsg from '../atoms/ErrorMsg'
import Label from '../atoms/Label'


export default (props) => {
    return (
        <>
          <Label
           id={props.id}
           label={props.label}
          />
          <Input
              type={props.type}
              placeholder={props.placeholder}
              id={props.id}
              // onChange={props.onChange}
              className={props.className}
              // value={props.value}
              {...props}
            />
          {props.errmsg && <ErrorMsg errMsg={props.errmsg}/>}
        </>
    )
}
