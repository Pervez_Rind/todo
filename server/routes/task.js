const ctrl = require('../controller/task')
const {validateBody} = require('../helper/middleware')

module.exports = (app) => {
    app.route('/task/:id?')
        .get(ctrl.get)
        .post(validateBody,ctrl.save)
        .put(ctrl.update)
        .delete(ctrl.remove)
            
}