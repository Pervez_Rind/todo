const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const todos = new Schema({
    todo: String,
    completed: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

exports.todos = () => mongoose.model('todos', todos);