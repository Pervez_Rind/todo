const services = require('../services/task')

const get = async (req,res) => {
    try {
    
        const data  = await services.getTodo()
        res.status(200).json({
            success:true,
            data,
            message:'Record Found Successfully'
        })
    } catch (e) {
        res.status(401).json({
            success:true,
            data:null,
            message:'Error Fetching Records'
        })
    }
}
const save = async (req,res) => {
    try {
        const body = req.body
        const query = {todo:req.body.todo}
        
        const exists = await services.getTodo(query)
        if (exists?.length) {
            res.status(409).json({
                success:false,
                data:null,
                message:'Already Exists'
            })
        }
        else {
            const data = await services.addTodo(body)
            res.status(200).json({
                success:true,
                data,
                message:'Task Added Successfully'
            })
        }

    } catch(e) {
        res.status(500).json({
            success:true,
            data:null,
            message:'Error Adding Task'
        })
    }
}
const update = async (req,res) => {
    try {
        const {id} = req.params
        const data = await services.updateTodo(id,req.body)
        res.status(200).json({
            success:true,
            data,
            message:'Task updated Successfully'
        })
    } catch(e) {
        res.status(500).json({
            success:false,
            error:e.stack,
            message:'Error Updating Task'
        })
    }
   
}
const remove = async (req,res) => {
    try {
        const query = {
            _id:req.params.id
        }
        const data = await services.deleteTodo(query)
        res.status(200).json({
            success:true,
            data,
            message:'Task Deleted Successfully'
        })
    } catch (e) {
        res.status(500).json({
            success:true,
            error:e,
            message:'Task Deleted Successfully'
        })
    }
}


module.exports = {get,save,update,remove}