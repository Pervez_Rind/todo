

const validateBody  =  async (req,res,next) => {
  
    if(!req.body.todo)
    res.status(403).json({
        success:false,
        data:[],
        message:'Required Data Missing'
    })
    else 
     next()

}

module.exports  = {validateBody}