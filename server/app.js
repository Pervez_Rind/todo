const express = require('express')

const app = express()
const port = process.env.port || 4000
// Express configuration
require('./config/express')(app)
// DB connection
require('./config/mongoose')()

const apiRoutes = express.Router();
app.use(apiRoutes);
require('./routes/task')(apiRoutes)

app.listen(port,()=> console.log(`server is listening on Port ${port}`))



