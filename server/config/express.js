
const morgan         = require('morgan');
const cors           = require('cors');
const express = require('express')

module.exports = (app) => {


    app.use(express.urlencoded({ extended: true}));
    app.use(express.json());
    app.use(morgan('dev'));
    app.use(cors());
    app.use( (req, res, next)=> {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,access_token,multipart/form-data');
        next()
    });

};