const TodoModel = require('../models/tasks').todos()

const addTodo = (body) => {
    return new Promise((resolve,reject) => {
        new TodoModel(body).save()
        .then((doc) => {
            console.log("Added",doc)
            resolve(doc)
        })
        .catch(e => {
            console.log("Failed to Add",e)
            reject(e)
        })
    })
}


const getTodo = (query) => {
    const findQuery = query || {}
    const select = {todo:1,_id:1}
    const sort = { sort:{createdAt: -1 }}
    
    return new Promise((resolve,reject) => {
        TodoModel.find(findQuery,select,sort)
        .then((doc) => {
            console.log("Found",doc)
            resolve(doc)
        })
        .catch(e => {
            console.log("Failed to fetch",e)
            reject(e)
        })
    })
}


const updateTodo = (_id,updateData) => {

    return new Promise((resolve,reject) => {
       
        TodoModel
        .findOneAndUpdate({_id}, updateData,{new:true})
        .then((doc) => {
            console.log("Removed",doc)
            resolve(doc)
        })
        .catch(e => {
            console.log("Failed to remove",e)
            reject(e)
        })
    })

}


const deleteTodo = (query) => {

    return new Promise((resolve,reject) => {
        TodoModel.deleteOne(query)
        .then((doc) => {
            console.log("Removed",doc)
            resolve(doc)
        })
        .catch(e => {
            console.log("Failed to remove",e)
            reject(e)
        })
    })
}


module.exports = {addTodo,getTodo,updateTodo,deleteTodo}